﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenClose : MonoBehaviour {
 
    GameObject wall;
    bool RightUp;
    bool LeftUp;

    // Use this for initialization
    void Start () {
    
    }

    void FixedUpdate () {
        UpdateWalls();
    }

    void UpdateWalls()
    {        
        wall = GameObject.FindWithTag("RightWall");
        if (Input.GetKey(KeyCode.L) && !LeftUp)
        {
            RightUp = true;
            if (wall.transform.localPosition.y < 2)
            {
                wall.transform.localPosition += new Vector3(0, .03f, 0);
            }

        }
        else
        {
            LowerWall(wall);
        }

        wall = GameObject.FindWithTag("LeftWall");
        if (Input.GetKey(KeyCode.J) && !RightUp)
        {
            LeftUp = true;
            if (wall.transform.localPosition.y < 2)
            {
                wall.transform.localPosition += new Vector3(0, .03f, 0);
            }

        }
        else
        {
            LowerWall(wall);
        }
    }

    void LowerWall(GameObject gO)
    {
        
        if (wall.transform.localPosition.y > 1)
        {
            wall.transform.localPosition -= new Vector3(0, .03f, 0);
        }

        if (wall.tag == "LeftWall")
        {
            LeftUp = false;
        }
        else
        {
            RightUp = false;
        }
    }
}
